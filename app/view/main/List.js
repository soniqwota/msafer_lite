Ext.define('lite.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',
    id: 'mainlist',

    requires: [
        'lite.store.Histori',
        'Ext.grid.plugin.Editable',
        'lite.view.main.MainController'
    ],

    plugins: [{
        type: 'grideditable'
    }],

    controller: "main",
    title: 'Histori Transaksi',

    bind: '{histori}',


    viewModel: {
    stores: {
      histori: {
        type: "histori",
      },
     },
    },

    columns: [
        //{ text: 'No',  dataIndex: 'no', width: 60 },
        { text: 'Tipe',  dataIndex: 'tipe', width: 100 },
        { text: 'Nominal', dataIndex: 'nominal', width: 100, editable: true },
        { text: 'Keterangan', dataIndex: 'keterangan', width: 230, editable: true },
        { text: 'Tanggal', dataIndex: 'tanggal', width: 230 },
        
    ],

  //   listeners:{
  //    select: 'onItemSelected'
  // },
});
