Ext.define('lite.view.form.Inputan', {
    extend: 'Ext.Container',
    xtype: 'inputan',

    requires: [
        'Ext.carousel.Carousel',
        'lite.view.form.Pendapatan',
        'lite.view.form.Pengeluaran'
    ],


    shadow: true,
    layout: {
        type: 'hbox',
    },
    defaults: {
        cls: 'demo-solid-background',
        flex: 1
    },
    items: [{
        xtype: 'carousel',
        direction: 'vertical',
        layout: 'fit',
        items: [{
           
                xtype: 'pendapatan'
        },
        {
           
                xtype: 'pengeluaran'
           
        }]
    }]
});