Ext.define('lite.view.form.Pengeluaran', {
    extend: 'Ext.form.Panel',
    xtype: 'pengeluaran',

    requires: [
    'Ext.form.FieldSet',
    'Ext.field.Number',
    'Ext.field.Select',
    'Ext.field.Hidden',
    'lite.store.Histori',
    ],

    viewModel: {
    stores: {
      histori: {
        type: "histori",
      },
     },
    },


    shadow: true,
    cls: 'demo-solid-background',
    id: 'form_pengeluaran',
    items: [
    {
        xtype: 'fieldset',
        id: 'pengeluaranset',
        title: 'Pengeluaran',
        instructions: 'Masukan jumlah pengeluaran anda.',
        defaults: {
            labelWidth: '35%'
        },
        items: [
        {
            xtype: 'selectfield',
            name: 'Nama Pengeluaran',
            id: 'namaPengeluaran',                    
            label: 'Rank',
            options: [
            {
                text: 'Konsumsi',
                value: 'Konsumsi'
            },
            {
                text: 'Kos-kosan',
                value: 'Kos-kosan'
            },
            {
                text: 'Transportasi',
                value: 'Transportasi'
            },
            {
                text: 'Pendidikan',
                value: 'Pendidikan'
            },
            {
                text: 'Hiburan',
                value: 'Hiburan'
            }

            ]
        },
        {
            xtype: 'textfield',
            name: 'nominalPengeluaran',
            id: 'nominalPengeluaran',
            label: 'nominalPengeluaran',
            placeHolder: 'Rp,-',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'textareafield',
            name: 'keteranganPengeluaran',
            id: 'keteranganPengeluaran',
            label: 'keteranganPengeluaran'
        }
        ]
    },
    {
        xtype: 'container',
        defaults: {
            xtype: 'button',
            style: 'margin: 1em',
            flex: 1
        },
        layout: {
            type: 'hbox'
        },

        items: [
        {
            ui:'action',
            margin: 10,
            text:'Submit',
            handler: function(){
                keterangan = Ext.getCmp('namaPengeluaran').getValue();
                nominal = Ext.getCmp('nominalPengeluaran').getValue();
                tipe = 'Pengeluaran';

                store = Ext.getStore('histori');
                store.beginUpdate();
                store.insert(0, {'tipe' : tipe, 'nominal' : nominal, 'keterangan' : keterangan});
                store.endUpdate();

                alert("data sudah di tambah");
                Ext.getCmp('keteranganPengeluaran').setValue("");
                Ext.getCmp('nominalPengeluaran').setValue("");

            }    
        },
        {
            text: 'Reset',
            ui: 'action',
            handler: function(){
                Ext.getCmp('basicform').reset();
            }
        }
        ]
    }
    ]
});