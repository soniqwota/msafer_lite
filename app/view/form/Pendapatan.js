Ext.define('lite.view.form.Pendapatan', {
    extend: 'Ext.form.Panel',
    xtype: 'pendapatan',

    requires: [
    'Ext.form.FieldSet',
    'Ext.field.Number',
    'Ext.field.Select',
    'Ext.field.Hidden',
    'lite.store.Histori',
    ],

    viewModel: {
    stores: {
      histori: {
        type: "histori",
      },
     },
    },


    shadow: true,
    cls: 'demo-solid-background',
    id: 'form_pendapatan',
    items: [
    {
        xtype: 'fieldset',
        id: 'pendapatanset',
        title: 'Pendapatan',
        instructions: 'Masukan jumlah pendapatan anda.',
        defaults: {
            labelWidth: '35%'
        },
        items: [
        {
            xtype: 'selectfield',
            name: 'namaPendapatan',
            id: 'namaPendapatan',
            label: 'Pendapatan',
            options: [
            {
                text: 'Gaji',
                value: 'gaji'
            },
            {
                text: 'uang jajan',
                value: 'uang jajan'
            }

            ]
        },
        {
            xtype: 'textfield',
            name: 'nominalPendapatan',
            id: 'nominalPendapatan',
            label: 'nominalPendapatan',
            placeHolder: 'Rp,-',
            autoCapitalize: true,
            required: true,
            clearIcon: true
        },
        {
            xtype: 'textareafield',
            name: 'keteranganPendapatan',
            id: 'keteranganPendapatan',
            label: 'keteranganPendapatan'
        }
        ]
    },
    {
        xtype: 'container',
        defaults: {
            xtype: 'button',
            style: 'margin: 1em',
            flex: 1
        },
        layout: {
            type: 'hbox'
        },
        items: [
        {
            ui:'action',
            text:'Submit',
            handler: function(){
                keterangan = Ext.getCmp('namaPendapatan').getValue();
                nominal = Ext.getCmp('nominalPendapatan').getValue();
                tipe = 'Pendapatan';

                store = Ext.getStore('histori');
                store.beginUpdate();
                store.insert(0, {'tipe' : tipe, 'nominal' : nominal, 'keterangan' : keterangan});
                store.endUpdate();

                alert("data sudah di tambah");
                Ext.getCmp('keteranganPendapatan').setValue("");
                Ext.getCmp('nominalPendapatan').setValue("");

            }    
        },
        {
            text: 'Reset',
            ui: 'action',
            handler: function(){
                Ext.getCmp('basicform').reset();
            }
        }
        ]
    }
    ]
});