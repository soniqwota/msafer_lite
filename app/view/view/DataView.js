Ext.define("lite.view.view.DataView", {
  extend: "Ext.Container",
  xtype: "bscdataview",

  requires: [
  "Ext.dataview.plugin.ItemTip",
  "Ext.plugin.Responsive",
  "lite.store.Personnel",
  'Ext.field.Search',
  ],
  
  viewModel: {
    stores: {
      personnel: {
        type: "personnel",
      },
    },
  },

  layout: "fit",
  cls: "ks-basic demo-solid-background",
  items: [
  {

    xtype: "dataview",
    scrollable: "y",
    cls: "dataview-basic",
    itemTpl:
    '<tr>'+
    '<td align="left" > <font size="5" color="green">P</font><font size="5"><font size="5">endapatan</font></td>'+
    '<td align="left" width="20px"> <font size="5"> &emsp;&nbsp;: </font></td>'+   
    '<td align="left" > <font size="5">{pendapatan}</font></td>'+    //tipe
    '</tr><br><br><br>'+    
    '<tr>'+
    '<td align="left" > <font size="5" color="red">P</font><font size="5">engeluaran</font></td>'+
    '<td align="left" width="20px"> <font size="5"> &emsp;: </font></td>'+   
    '<td align="left" > <font size="5">{pengeluaran}</font></td>'+    //tipe
    '</tr><br><br><br>'+    
    '<tr>'+
    '<td align="left" > <font size="5" color="grey">B</font><font size="5"><font size="5">alance</font></td>'+
    '<td align="left" width="20px"> <font size="5"> &emsp;&emsp;&emsp;: </font></td>'+   
    '<td align="left" > <font size="5">{balance}</font></td>'+                             
    '</tr><br><br><br>',

    bind: {
      store: "{personnel}",
    },

    plugins: {
      type: "dataviewtip",
      align: "l-r?",
      plugins: "responsive",

        // On small form factor, display below.
        responsiveConfig: {
          "width < 600": {
            align: "tl-bl?",
          },
        },
        width: 600,
        minWidth: 300,
        // delegate: "img",
        allowOver: true,
        anchor: true,
        bind: "{record}",
        tpl:
        '<table style="border-spacing:3px;border-collapse:separate">' +
        "<tr><td><font color='blue'>{note} : {nilai}</font></td></tr>" +
        '</table>',
      },
    },

    ],
  });
