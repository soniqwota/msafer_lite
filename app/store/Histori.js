Ext.define('lite.store.Histori', {
    extend: 'Ext.data.Store',

    alias: 'store.histori',
    storeId: 'histori',
    autoLoad : true,
    autoSync : true,

    fields: [
        'no','tipe', 'nominal', 'tanggal', 'keterangan', 'pendapatan', 'pengeluaran', 'balance'
    ],

    proxy: {
        type: 'jsonp',
        api :{
            read :"http://localhost/msafer/histori_msafer.php",
            update :"http://localhost/msafer/update_msafer.php",
            destroy :"http://localhost/msafer/destroy_msafer.php",
            create :"http://localhost/msafer/add_msafer.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
