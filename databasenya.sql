/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.14-MariaDB : Database - msafer
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`msafer` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `msafer`;

/*Table structure for table `histori` */

DROP TABLE IF EXISTS `histori`;

CREATE TABLE `histori` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` char(15) DEFAULT NULL,
  `nominal` float DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`no`),
  KEY `no` (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

/*Data for the table `histori` */

insert  into `histori`(`no`,`tipe`,`nominal`,`tanggal`,`keterangan`) values 
(1,'Pendapatan',1300000,'2020-11-28','gaji lembur'),
(2,'Pengeluaran',50000,'2020-11-28','makan'),
(3,'Pendapatan',1250000,'2020-11-28','gaji biasa'),
(5,'Pendapatan',100000,'2020-12-05','gaji tambahan'),
(6,'Pendapatan',20000,'2020-12-05','jajan'),
(7,'Pendapatan',50000,'2020-12-05','ketemu uang di jalan'),
(8,'Pengeluaran',15000,'2020-12-05','makan siang'),
(9,'Pengeluaran',10500,'2021-01-21','mamam'),
(14,'Pengeluaran',2000,'2021-01-21','pangan'),
(15,'Pengeluaran',1000,'2021-01-21','pangan');

/*Table structure for table `info` */

DROP TABLE IF EXISTS `info`;

CREATE TABLE `info` (
  `id` char(10) NOT NULL,
  `tipe` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `info` */

insert  into `info`(`id`,`tipe`) values 
('bal','Balance'),
('in','Pendapatan'),
('out','Pengeluaran');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
